import ThinUI from './thinui.js';
import { loadedAssets } from './assets.js';

function sortSprites(l, r) { return ((l.z || 0) - (r.z || 0)) || (l.y - r.y) || (l.x - r.x); }

export default class Scene extends ThinUI.fromHtml('<canvas width="768" height="432"></canvas>') {
  static instance = null;
  static ui = null;

  constructor(props, root) {
    super(props, root);
    Scene.instance = this;
    window.scene = this;
    this.ctx = this.root.getContext('2d');
    this.ctx.imageSmoothingEnabled = false;
    this.sprites = [
      { img: 'background', x: 15, y: -14, z: -1, dirty: true },
      { img: 'desk-back', x: 63, y: 18 },
      { img: 'desk-front', x: 59, y: 37, z: 5 },
      { img: 'coffeepot-top', x: 72, y: 18, z: 4 },
    ];
    this.tickHandlers = [];
    this._lastTick = null;
    this.tick = this.tick.bind(this);
    this.pause = this.pause.bind(this);
    this.paused = false;
    this._raf = null;
  }

  attach(ui) {
    Scene.ui = ui;
  }

  async start() {
    if (Scene.ui.fullscreen.root.checked) {
      this.paused = true;
      try {
        await Scene.ui.gameFS.root.requestFullscreen();
      } catch (err) {
        // Couldn't go fullscreen, but that's OK
        console.log(err);
      }
    }
    this.paused = false;
    Scene.ui.titleName.root.innerText = 'All-Nighter Simulator';
    Scene.ui.pauseButton.root.innerText = 'Pause';
    this._lastTick = performance.now();
    if (!this._raf) {
      this._raf = window.requestAnimationFrame(this.tick);
    }
    if (Scene.ui.enableBgm.root.checked) {
      Scene.ui.bgm.root.volume = 0.6;
      if (this.gameOver) {
        Scene.ui.bgm.root.currentTime = 0;
      }
      Scene.ui.bgm.root.play();
    }
    this.gameOver = false;
    if (Scene.ui.enableSfx.root.checked) {
      for (const key of ['pourFx', 'brewFx']) {
        const fx = Scene.ui[key].root;
        if (fx.currentTime > 0 && !fx.ended) {
          fx.play();
        }
      }
    }
  }

  pause(toggle = false) {
    if (this.gameOver) {
      return;
    }
    if (!this.paused) {
      if (this._raf) {
        window.cancelAnimationFrame(this._raf);
        this._raf = null;
      }

      this.paused = true;
      ui.titleName.root.innerText = 'Paused...';
      ui.pauseButton.root.innerText = 'Resume';
      document.exitFullscreen().catch(() => {});
      if (Scene.ui.enableBgm.root.checked) {
        Scene.ui.bgm.root.pause();
      }
      if (Scene.ui.enableSfx.root.checked) {
        for (const key of ['pourFx', 'brewFx']) {
          const fx = Scene.ui[key].root;
          if (fx.currentTime > 0 && !fx.ended) {
            fx.pause();
          }
        }
      }
    } else if (toggle) {
      this.resume();
    }
  }

  resume() {
    if (this.paused === true) {
      this.start();
    }
  }

  registerTick(handler) {
    this.tickHandlers.push(handler);
  }

  tick(timestamp) {
    if (this.paused || ui.title.isVisible) {
      if (this._raf) {
        window.cancelAnimationFrame(this._raf);
        this._raf = null;
      }
      return;
    }
    const ms = timestamp - this._lastTick;
    this._lastTick = timestamp;
    for (const handler of this.tickHandlers) {
      handler.tick(ms);
    }
    let shouldRedraw = false;
    let shouldSort = false;
    for (const obj of this.sprites) {
      if (obj.dirty) {
        shouldRedraw = true;
        shouldSort = shouldSort || (obj.dirty === 2);
        obj.dirty = false;
      }
    }
    if (shouldSort) {
      this.sprites.sort(sortSprites);
    }
    if (shouldRedraw) {
      this.redraw();
    }
    this._raf = window.requestAnimationFrame(this.tick);
  }

  addSprite(sprite) {
    this.addSprites([sprite]);
  }

  addSprites(sprites) {
    if (sprites.length) {
      sprites[0].dirty = 2;
      this.sprites.push(...sprites);
    }
  }

  redraw() {
    this.ctx.clearRect(0, 0, 768, 432);
    for (const obj of this.sprites) {
      if (obj.hide) continue;
      const i = loadedAssets[obj.img];
      this.ctx.drawImage(i, (-15+obj.x) * 4, (14+obj.y) * 4, i.width * 4, i.height * 4);
    }
  }
};
window.Scene = Scene;
ThinUI.register(Scene);
