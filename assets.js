const sources = [
  'background',
  'desk-front',
  'desk-back',
  'coffeepot-top',
  'coffeepot-full',
  'coffeepot-75',
  'coffeepot-50',
  'coffeepot-25',
  'coffeepot-0',
  'coffeepot-fill',
  'coffeepot-pour-full',
  'coffeepot-pour-75',
  'coffeepot-pour-50',
  'coffeepot-pour-25',
  'coffeepot-pour-0',
  'mug',
  'mug-upset',
  'chair',
  'brew',
  'type',
  'fill',
  'drink',
  'brew-dis',
  'type-dis',
  'fill-dis',
  'drink-dis',
  'upper-sit',
  'upper-type1',
  'upper-type2',
  'upper-type3',
  'upper-drink1',
  'upper-drink2',
  'upper-drink3',
  'upper-sleep1',
  'upper-sleep2',
  'stand-brew1',
  'stand-brew2',
  'stand-fill1',
  'stand-fill2',
  'stand-fill3',
  'lower-sit',
  'blink',
  'frown',
];

export const loadedAssets = {};

function fetchAsset(name) {
  const img = document.createElement('IMG');
  return new Promise((resolve, reject) => {
    img.addEventListener('load', () => {
      loadedAssets[name] = img;
      resolve(img);
    });
    img.addEventListener('error', () => reject(new Error(`unable to load '${name}'`)));
    img.src = name + '.png';
  });
}

export default Promise.all(sources.map(fetchAsset)).then(() => loadedAssets);
