Ludum Dare 50
-------------

**Theme:** Delay the Inevitable

**Team:** [Adam Higerd](https://ldjam.com/users/coda-highland) and [Ginny Higerd](https://ldjam.com/users/immamoonkin)

All-Nighter Simulator
=====================

It's 9:00 at night, and your research paper is due in the morning... but you haven't even started on the report!

Time to boot up the computer, brew a pot of coffee, and get to work. You can only hope that enough caffeine can delay the inevitable exhaustion long enough to finish.

![All-Nighter Simulator](preview.png)

I forced a bot to read thousands of words from reports about sleep, caffeine, and cognitive function and then asked it to do my homework. This is what happened.

Try to see how long you can stay awake and how many words you can write before you finally crash!

[Play on the Web (HTML5)](http://greenmaw.com/webgames/ld50/)

Controls
--------
Use the touchscreen or the mouse to tap the buttons on the left side, or use the keyboard shortcuts:

* T: Type a word
* B: Brew a new pot of coffee
* F: Fill your mug
* D: Drink some coffee

An auto-type feature is provided if it's too burdensome for you to mash the "Type" command.

Coffee will perk you up for a little while, but the more you drink, the less benefit you derive from it.

License
=======
Software copyright (c) 2022 Adam Higerd

Sound and music copyright (c) 2022 Adam Higerd

Graphics copyright (c) 2022 Adam and Ginny Higerd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
