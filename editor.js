import ThinUI, { ThinUIComponent } from './thinui.js';
import Scene from './scene.js';

let dictionary = null;
export const loadDictionary = fetch('dictionary.json').then(res => res.json()).then(d => dictionary = d);
const banwords = {};
const banctx = {};

const equivs = {
  'and': 'and',
  'or': 'and',
  'and/or': 'and',
  'girls': 'children',
  'boys': 'children',
  'sleepy': 'fatigued',
  'sleepiness': 'fatigue',
  'tiredness': 'fatigue',
};

const rEquivs = {
  'and': ['and', 'or', 'and/or'],
  'children': ['children', 'boys', 'girls'],
  'fatigued': ['sleepy', 'fatigued', 'tired'],
  'fatigue': ['sleepiness', 'fatigue', 'tiredness'],
};

function isCommaNumber(w) {
  return /^(\d+,)*\d+(\.\d+)?$/.exec(w) && true;
}

function normalize(w) {
  if (w == '.') {
    return '.';
  } else if (isCommaNumber(w) || w == '#') {
    return '#';
  } else if (w == ',') {
    return ',';
  }
  const n = w.toLowerCase().replace(/[^a-z']/g, '');
  return equivs[n] || n;
}

function generateSentence() {
  let s = [];
  const used = {};
  let ctx = [];
  let w = null;
  while (w != '.' && s.length < 100) {
    const len = s.length;
    const vs = {};
    let numBase = null;
    let follow = {};
    let tw = 0;
    for (let i = 0; i < ctx.length; i++) {
      const hist = ctx.slice(i).join('_');
      follow = dictionary[hist] || {};
      const numFollow = Object.keys(follow).length;
      if (numFollow < 1 || (numFollow < 2 && i < ctx.length - 1)) {
        continue;
      }
      for (let k in follow) {
        if (banwords[k]) {
          continue;
        } else if (k == '.') {
          if (len < 5) {
            continue;
          } else if (len && s[s.length - 1] == ',') {
            continue;
          } else if (len > 30) {
            return s;
          }
        }
        const nk = normalize(k);
        if (len > 10 && used[nk]) {
          continue;
        } else if (banctx[ctx.join('_') + '_' + nk]) {
          continue;
        }
        if (numBase === null && nk == '#') {
          numBase = k;
        }
        vs[k] = follow[k];
      }
      tw = 0;
      for (let k in vs) {
        tw += vs[k];
      }
      if (tw > 100) {
        break;
      }
    }
    if (!tw) {
      // dead end, backtrack
      banctx[ctx.join('_')] = true;
      if (len < 2) {
        w = null;
        s = [];
        ctx = ['.'];
      } else {
        s.pop();
        w = s[s.length - 1];
        ctx = ctx.slice(0, -1);
      }
      continue;
    }
    let r = (Math.random() ** 2) * tw;
    for (let k in vs) {
      r -= follow[k];
      if (r < 0) {
        w = normalize(k);
        ctx.push(w);
        if (w == '#') {
          let val = Number(numBase) * (0.9 + 0.2 * Math.random());
          if (val < 0) {
            val = 0;
          }
          if (!numBase.includes('.')) {
            s.push((val|0).toLocaleString('en-US'));
          } else {
            s.push((Math.round(val * 10) / 10).toLocaleString('en-US'));
          }
        } else if (rEquivs[w]) {
          s.push(rEquivs[w][(Math.random() * rEquivs[w].length)|0]);
        } else if (w != '.') {
          s.push(k);
        }
        if (w.length > 5) {
          used[w] = true;
        }
        break;
      }
    }
  }
  if (s.length < 3) {
    return generateSentence();
  }
  return s;
}

function formatSentence(s) {
  s = s.join(' ') + '.';
  s = s.replace(/ ,/g, ',').replace(/ '/g, "'");
  s = s[0].toUpperCase() + s.substr(1);
  return s;
}
window.generateSentence = () => formatSentence(generateSentence());

class Editor extends ThinUIComponent {
  constructor(props, root) {
    super(props, root);
    this.addWord = this.addWord.bind(this);
    window.setInterval(this.flashCaret.bind(this), 500);
    window.editor = this;
    this.reset();
  }

  reset() {
    this.content.innerText = '';
    this.words = 0;
    this.elapsedTime = 0;
    this.lastMin = -1;
    this.currentSentence = [];
    this.paragraphLen = Math.round(Math.random() * 4 + 3);
    this.autoTypeDelay = 0;
    this.animState = 0;
    if (Scene.ui) {
      Scene.ui.typeButton.syntheticOnly = Scene.ui.autoType.root.checked;
    }
  }

  flashCaret() {
    if (!Scene.instance.paused) {
      this.caret.classList.toggle('blink');
    }
  }

  addWord() {
    if (this.currentSentence.length == 0) {
      this.currentSentence = formatSentence(generateSentence()).split(' ');
    }
    const word = this.currentSentence.shift();
    if (this.content.innerText == '') {
      this.content.innerText = '\xa0\xa0\xa0\xa0\xa0' + word;
    } else {
      this.content.innerText += ' ' + word;
    }
    this.words++;
    if (word.endsWith('.')) {
      this.paragraphLen--;
      if (this.paragraphLen <= 0) {
        this.content.innerText += '\n\n\xa0\xa0\xa0\xa0\xa0';
        while (this.contentFrame.scrollHeight > 10 * this.contentFrame.offsetHeight) {
          const breakPos = this.content.innerText.indexOf('\n');
          this.content.innerText = this.content.innerText.substr(breakPos + 2);
        }
        this.paragraphLen = Math.round(Math.random() * 4 + 3);
      }
    }
    const page = 1 + Math.floor(this.words / 250);
    this.pageCount.innerText = `Page ${page}/${page}`;
    this.wordCount.innerText = `Words: ${this.words}`;
    this.updateScroll();
    this.animState = 1;

    if (Scene.ui.enableSfx.root.checked) {
      const fx = Math.floor(Math.random() * 3) + 1;
      const el = Scene.ui['typeFx' + fx].root;
      el.currentTime = 0;
      el.volume = 0.3;
      el.play();
    }
  }

  addZ() {
    this.content.innerText += 'z';
    this.updateScroll();
  }

  updateScroll() {
    this.contentFrame.scrollTop = this.contentFrame.scrollHeight;
    this.scrollbar.classList.toggle('disabled', this.contentFrame.scrollTop == 0);
  }

  tick(ms) {
    if (Scene.ui.autoType.root.checked && !Scene.ui.typeButton.disabled) {
      this.autoTypeDelay -= ms;
      if (this.autoTypeDelay < 0) {
        this.autoTypeDelay += 200;
        Scene.ui.typeButton.click();
      }
    }
    if (this.animState > 0) {
      this.animState += ms;
      if (this.animState >= 150) {
        this.animState = 0;
      }
    }
    // 3 seconds per minute, start at 9PM
    this.elapsedTime += ms;
    const totalMin = (this.elapsedTime / 3000 + 540)|0;
    if (totalMin == this.lastMin) {
      return;
    }
    this.lastMin = totalMin;
    const min = ('0' + (totalMin % 60)).substr(-2);
    let hour = Math.floor(totalMin / 60);
    const am = (hour % 24) > 11;
    if (am) {
      hour -= 12;
    }
    this.clock.innerText = `${hour || 12}:${min} ${am ? 'AM' : 'PM'}`;
  }
};
ThinUI.register(Editor);
