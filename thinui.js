class BareThinUIComponent {
  constructor(root) {
    if (root) {
      this.root = root;
    } else {
      this.root = this._template.cloneNode(true);
    }
    this.root.tui = this;
  }

  appendTo(parent) {
    if (parent.root) {
      parent.root.appendChild(this.root);
    } else {
      parent.appendChild(this.root);
    }
  }

  replaceElement(elem) {
    elem.parentNode.insertBefore(this.root, elem);
    elem.parentNode.removeChild(elem);
  }

  get className() {
    return this.root.className;
  }

  set className(v) {
    this.root.className = v;
  }

  get classList() {
    return this.root.classList;
  }

  get style() {
    return this.root.style;
  }

  get isVisible() {
    return this.root.style.display !== 'none';
  }

  set isVisible(show) {
    this.root.style.display = show ? '' : 'none';
  }

  hide() {
    this.isVisible = false;
  }

  show() {
    this.isVisible = true;
  }

  focus() {
    this.root.focus();
  }

  blur() {
    this.root.blur();
  }
}

export class ThinUIComponent extends BareThinUIComponent {
  constructor(props, root = null) {
    super(root);
    this._initChildren();

    const proto = Object.getPrototypeOf(Object.getPrototypeOf(this));
    for (const key in props) {
      if (this._tuiProperties.has(key)) {
        this[key] = props[key];
      }
    }
  }

  _initChildren() {
    this._tui = [...this.root.querySelectorAll('[data-tui-index],[id]')];
    for (const child of this._tui) {
      if (child.dataset.tuiClass) {
        const tuiClass = ThinUI[child.dataset.tuiClass];
        console.log(child.dataset.tuiClass);
        const widget = new tuiClass(child.dataset, child);
        widget._initChildren();
        this[child.dataset.tuiId] = widget;
      } else if (child.dataset.tuiId) {
        this[child.dataset.tuiId] = child;
      } else if (child.id) {
        this[child.id] = child;
      }
    }
    if ('tuiIndex' in this.root.dataset) {
      this._tui.unshift(this.root);
    }
    for (const childID in this._events) {
      const child = this._tui[childID];
      const childEvents = this._events[childID];
      for (const method in childEvents) {
        child.addEventListener(childEvents[method], (event) => { this[method] && this[method](event); });
      }
    }
  }
}
ThinUIComponent.prototype._tuiProperties = new Set(['isVisible']);

const implClasses = {};

/* internal */
function makeUIClass(root, id, impl = null) {
  init(root);
  const events = {};
  if (!impl) {
    impl = ThinUIComponent;
  }
  const cls = (new Function(
    'impl',
    `return class ${id || ''} extends impl.${impl.name} {}`,
  ))({ [impl.name]: impl });
  if (root.childElementCount == 1) {
    cls.prototype._template = root.firstElementChild;
  } else {
    const div = document.createElement('DIV');
    for (const child of root.children) {
      div.appendChild(child.cloneNode(true));
    }
    cls.prototype._template = div;
  }
  cls.prototype._events = events;
  cls.prototype._tuiProperties = new Set(impl.prototype._tuiProperties || []);
  if (impl !== ThinUIComponent) {
    for (const attr of impl.props || []) {
      cls.prototype._tuiProperties.add(attr);
    }
    for (const attr of Reflect.ownKeys(impl.prototype)) {
      if (attr === 'constructor') {
        continue;
      }
      cls.prototype._tuiProperties.add(attr);
    }
  }
  let tag = 0;
  for (const child of root.querySelectorAll('[data-tui-properties],[data-tui-events],[data-tui-id],[id]')) {
    if (child.id && !child.dataset.tuiId) {
      child.dataset.tuiId = child.id;
      child.removeAttribute('id');
    }
    const childID = tag++;
    child.dataset.tuiIndex = childID;
    const bindEvents = events[childID] = {};
    if (child.dataset.tuiProperties) {
      for (const prop of child.dataset.tuiProperties.split(',')) {
        let [exposeName, innerName] = prop.split('=');
        innerName = innerName || exposeName;
        Object.defineProperty(cls.prototype, exposeName, {
          get: function() { return this._tui[childID][innerName]; },
          set: function(v) { return this._tui[childID][innerName] = v; console.log(childID, innerName, v) },
        });
        cls.prototype._tuiProperties.add(exposeName);
      }
      delete child.dataset.tuiProperties;
    }
    if (child.dataset.tuiEvents) {
      for (const prop of child.dataset.tuiEvents.split(',')) {
        let [exposeName, eventName] = prop.split('=');
        eventName = (eventName || exposeName).toLowerCase();
        exposeName = 'on' + exposeName[0].toUpperCase() + exposeName.substr(1);
        bindEvents[exposeName] = eventName;
      }
      delete child.dataset.tuiEvents;
    }
  }
  return cls;
}

export function register(cls) {
  implClasses[cls.name] = cls;
}

export function scan(root) {
  if (!root.getElementsByTagName) {
    return;
  }
  for (const tpl of [...root.getElementsByTagName('template')]) {
    if (tpl.id && !ThinUI[tpl.id]) {
      ThinUI[tpl.id] = makeUIClass(tpl.content, tpl.id, implClasses[tpl.id]);
    }
  }
}

export function init(root, id) {
  scan(root);

  const children = {};
  if (!id && root.tagName == 'BODY') {
    id = 'document.body';
  }

  for (const child of root.querySelectorAll('ThinUI,[data-tui-class]')) {
    const tuiClass = child.dataset.tuiClass && (ThinUI[child.dataset.tuiClass] || implClasses[child.dataset.tuiClass]);
    if (!tuiClass) {
      console.error(`Unknown ThinUI element ${child.dataset.tuiClass || '(missing)'} in ${id || 'fromHtml'}`);
      continue;
    }
    let widget;
    const props = {
      ...child.dataset,
    };
    if (child.tagName == 'THINUI') {
      if (child.childNodes.length) {
        props.domContent = child.childNodes;
      }
      widget = new tuiClass(props);
      widget.replaceElement(child);
    } else {
      widget = new tuiClass(props, child);
    }
    widget.root.setAttribute('id', child.id);
    widget.root.dataset.tuiClass = child.dataset.tuiClass;
    if (widget && widget.root && widget.root.getAttribute('id')) {
      children[widget.root.getAttribute('id')] = widget;
    }
  }

  for (const child of root.querySelectorAll('[id]')) {
    if (child.id && !(child.id in children)) {
      children[child.id] = new BareThinUIComponent(child);
    }
  }

  return children;
}

export function fromHtml(html, className = null) {
  const tpl = document.createElement('TEMPLATE');
  tpl.innerHTML = html;
  return makeUIClass(tpl.content.cloneNode(true), className);
}

const ThinUI = {
  ThinUIComponent,
  register,
  scan,
  init,
  fromHtml,
};
export default ThinUI;
