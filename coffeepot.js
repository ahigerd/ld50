import Scene from './scene.js';

const frames = [
  'coffeepot-0',
  'coffeepot-25',
  'coffeepot-50',
  'coffeepot-75',
  'coffeepot-full',
  'coffeepot-full',
];

export const FILLING = Symbol('filling');
export const POURING = Symbol('pouring');
export const IDLE = Symbol('idle');

let ui = null;

const potPos = {
  idle: { x: 73, y: 25, z: 2 },
  pourFull: { img: 'coffeepot-pour-full', x: 79, y: 16, z: 4.1 },
  pour75: { img: 'coffeepot-pour-75', x: 79, y: 16, z: 4.1 },
  pour50: { img: 'coffeepot-pour-50', x: 79, y: 16, z: 4.1 },
  pour25: { img: 'coffeepot-pour-25', x: 79, y: 16, z: 4.1 },
  pour0: { img: 'coffeepot-pour-0', x: 79, y: 16, z: 4.1 },
};

const mugPos = {
  idle: { img: 'mug', x: 65, y: 64, z: 5.8, hide: false },
  fill: { img: 'mug', x: 83, y: 26, z: 4.1, hide: false },
  drink1: { x: 71, y: 41, hide: true },
  drink2: { x: 70, y: 40, hide: true },
  sleep: { img: 'mug-upset',  x: 61, y: 65, z: 5.8, hide: false },
};

const chairPos = {
  sit: { x: 58, y: 38, z: 2 },
  stand: { x: 48, y: 39, z: 4.5 },
};

const lowerPos = {
  sit: { img: 'lower-sit', x: 60, y: 53, hide: false },
  stand: { hide: true },
};

const upperPos = {
  sit: { img: 'upper-sit', x: 57, y: 29, z: 6 },
  type1: { img: 'upper-type1', x: 57, y: 29, z: 6 },
  type2: { img: 'upper-type2', x: 57, y: 29, z: 6 },
  type3: { img: 'upper-type3', x: 57, y: 29, z: 6 },
  drink1: { img: 'upper-drink1', x: 57, y: 29, z: 8 },
  drink2: { img: 'upper-drink3', x: 57, y: 29, z: 6 },
  drink3: { img: 'upper-drink2', x: 57, y: 29, z: 6 },
  brew1: { img: 'stand-brew1', x: 61, y: 8, z: 4.2 },
  brew2: { img: 'stand-brew2', x: 61, y: 8, z: 4.2 },
  fill1: { img: 'stand-fill1', x: 61, y: 8, z: 4.2 },
  fill2: { img: 'stand-fill2', x: 61, y: 8, z: 4.2 },
  fill3: { img: 'stand-fill3', x: 61, y: 8, z: 4.2 },
  sleep1: { img: 'upper-sleep1', x: 58, y: 30, z: 6 },
  sleep2: { img: 'upper-sleep2', x: 58, y: 30, z: 6 },
};

class CoffeePot {
  constructor() {
    this.pot = { img: 'coffeepot-0', x: 73, y: 25, z: 2 };
    this.fill = { img: 'coffeepot-fill', x: 73, y: 25, z: 3, hide: true };
    this.mug = { img: 'mug', x: 68, y: 64, z: 5.8 };
    this.chair = { img: 'chair', x: 58, y: 38, z: 2 };
    this.lower = { ...lowerPos.sit, z: 3.8 };
    this.upper = { ...upperPos.sit };
    this.blink = { img: 'blink', hide: true, x: 67, y: 37, z: this.upper.z + 1 };
    this.frown = { img: 'frown', hide: false, x: 69, y: 41, z: this.upper.z + .1 };
    this.sprites = [
      this.pot,
      this.fill,
      this.mug,
      this.chair,
      this.lower,
      this.upper,
      this.blink,
      this.frown,
    ];
    this.state = IDLE;
  }

  _moveSprite(sprite, pos) {
    for (const key in pos) {
      if (sprite[key] != pos[key]) {
        const resort = 'z' in pos ? sprite.z != pos.z : false;
        Object.assign(sprite, pos);
        sprite.dirty = (sprite.dirty === 2 || resort) ? 2 : 1;
        return;
      }
    }
  }

  _updateMug() {
    let mugTo = mugPos.idle;
    if (this.drinking > 0.2 && this.drinking < 1) {
      mugTo = (this.drinking * 5) % 2 >= 1 ? mugPos.drink1 : mugPos.drink2;
    } else if (this.state == POURING && ui.coffeeMeter.value >= .15 && ui.coffeeMeter.value <= .9) {
      mugTo = mugPos.fill;
    }
    this._moveSprite(this.mug, mugTo);
  }

  _updateChair() {
    let chairTo = chairPos.stand;
    if (this.state === POURING && (ui.coffeeMeter.value < .15 || ui.coffeeMeter.value > .9)) {
      chairTo = chairPos.sit;
    } else if (this.state === IDLE || this.busy <= 0 || this.drinking > 0 || this.asleep) {
      chairTo = chairPos.sit;
    }
    this._moveSprite(this.chair, chairTo);
  }

  _updatePlayer(ms) {
    let upperTo, lowerTo = lowerPos.sit;
    if (this.asleep) {
      if (this.busy > 9000) {
        upperTo = upperPos.sleep1;
      } else {
        upperTo = upperPos.sleep2;
        this._moveSprite(this.mug, mugPos.sleep);
      }
    } else if (this.drinking >= 1) {
      upperTo = upperPos.drink1;
    } else if (this.drinking > 0) {
      if (this.drinking > 0.2) {
        upperTo = (this.drinking * 5) % 2 >= 1 ? upperPos.drink2 : upperPos.drink3;
      } else {
        upperTo = upperPos.drink1;
      }
    } else if (this.state === IDLE || this.busy <= 0) {
      if (ui.doc.animState >= 100) {
        upperTo = upperPos.type3;
      } else if (ui.doc.animState >= 50) {
        upperTo = upperPos.type2;
      } else if (ui.doc.animState > 0) {
        upperTo = upperPos.type1;
      } else {
        upperTo = upperPos.sit;
      }
    } else if (this.state === FILLING) {
      lowerTo = lowerPos.stand;
      if (this.busy <= 250 || this.busy >= 750) {
        upperTo = upperPos.brew1;
      } else {
        if (this._brewStart && ui.enableSfx.root.checked) {
          ui.brewFx.root.volume = 0.3;
          ui.brewFx.root.currentTime = 0;
          ui.brewFx.root.play();
          this._brewStart = false;
        }
        upperTo = upperPos.brew2;
      }
    } else if (this.state === POURING) {
      lowerTo = lowerPos.stand;
      let potTo = potPos.idle;
      const v = ui.coffeeMeter.value;
      if (v < .15 || v > .90) {
        lowerTo = lowerPos.sit;
        upperTo = upperPos.drink1;
      } else if (v < .25) {
        upperTo = upperPos.fill1;
      } else if (v < .35 || v > .80) {
        upperTo = upperPos.fill2;
      } else {
        if (this._fillStart && ui.enableSfx.root.checked) {
          ui.pourFx.root.volume = 0.5;
          ui.pourFx.root.currentTime = 0;
          ui.pourFx.root.play();
          this._fillStart = false;
        }
        upperTo = upperPos.fill3;
        const l = ui.brewMeter.value;
        if (l > .85) {
          potTo = potPos.pourFull;
        } else if (l > .60) {
          potTo = potPos.pour75;
        } else if (l > .35) {
          potTo = potPos.pour50;
        } else if (l > .10) {
          potTo = potPos.pour25;
        } else {
          potTo = potPos.pour0;
        }
      }
      this._moveSprite(this.pot, potTo);
      this._updateChair();
      this._updateMug();
    }
    if (lowerTo) {
      this._moveSprite(this.lower, lowerTo);
    }
    if (upperTo) {
      this._moveSprite(this.upper, upperTo);
    }
    if (this.upper.x == upperPos.sit.x && this.upper.y == upperPos.sit.y) {
      const drowsy = ui.energyMeter.value < .25;
      if (drowsy != !this.frown.hide) {
        this.frown.hide = !drowsy;
        this.frown.dirty = true;
      }
      if (this.asleep) {
        this._blink = 0;
      } else {
        this._blink -= ms;
      }
      if (this._blink < -150) {
        this.blink.hide = true;
        this.blink.dirty = true;
        this._blink = Math.random() * 2000 + 1000;
      } else if (this._blink <= 0 && this.blink.hide) {
        this.blink.hide = false;
        this.blink.dirty = true;
      }
    } else {
      if (!this.frown.hide) {
        this.frown.hide = true;
        this.frown.dirty = true;
      }
      if (!this.blink.hide) {
        this.blink.hide = true;
        this.blink.dirty = true;
      }
    }
  }

  _updateState(newState = null) {
    if (newState) {
      this.state = newState;
    }
    this._updateMug();
    this._updateChair();
    if (this.busy > 0 || this.asleep) {
      ui.typeButton.disabled = true;
      ui.brewButton.disabled = true;
      ui.fillButton.disabled = true;
      ui.drinkButton.disabled = true;
      return;
    }
    ui.typeButton.disabled = false;
    ui.brewButton.disabled = this.state !== IDLE || ui.brewMeter.value > 0;
    ui.fillButton.disabled = this.state !== IDLE || ui.brewMeter.value <= 0 || ui.coffeeMeter.value > 0;
    ui.drinkButton.disabled = ui.coffeeMeter.value <= 0;
  }

  startBrew() {
    ui.scene.resume();
    if (this.state === IDLE && ui.brewMeter.value <= 0) {
      this._brewStart = true;
      this.busy = 1000;
      this._updateState(FILLING);
    }
  }

  startFill() {
    ui.scene.resume();
    if (this.state === IDLE && ui.coffeeMeter.value <= 0) {
      this._fillStart = true;
      this.busy = 1000;
      this._updateState(POURING);
    }
  }

  startDrink() {
    ui.scene.resume();
    if (ui.coffeeMeter.value > 0 && this.drinking < 0) {
      ui.drinkButton.disabled = true;
      this.drinking = 1.2;
      this.busy = 1200;
      this._updateState();
    }
  }

  attach(_ui) {
    ui = _ui;
    ui.scene.addSprites(this.sprites);
    ui.scene.registerTick(this);
    ui.brewButton.onClick = this.startBrew.bind(this);
    ui.fillButton.onClick = this.startFill.bind(this);
    ui.drinkButton.onClick = this.startDrink.bind(this);
    this.reset();
  }

  reset() {
    this.drinking = -1;
    this.caffeine = 0;
    this.tolerance = .999;
    this.busy = 0;
    this.asleep = false;
    this._blink = Math.random() * 2000 + 1000;
    ui.brewMeter.value = 0;
    ui.coffeeMeter.value = 0;
    ui.energyMeter.value = 1;
    this._updateState(IDLE);
  }

  tick(ms) {
    if (this.caffeine > 0) {
      let delta = ms * .00002;
      if (this.caffeine < 0.03) {
        delta *= .5;
      }
      this.caffeine -= delta / this.tolerance;
      ui.energyMeter.value += delta * this.tolerance;
      this.tolerance *= 1.0 - delta;
    } else {
      ui.energyMeter.value -= ms * .00002 * (0.7 + ui.doc.elapsedTime / 360000);
      if (ui.energyMeter.value <= 0) {
        if (this.asleep) {
          this.busy -= ms;
          if (this.busy > 0) {
            ui.doc.addZ();
            ui.scene.style.opacity = this.busy / 10000;
            ui.doc.style.opacity = this.busy / 10000;
            ui.status.style.opacity = this.busy / 10000;
            ui.bgm.root.volume = 0.6 * this.busy / 10000;
            ui.brewFx.root.volume = 0.3 * this.busy / 10000;
          } else {
            ui.scene.style.opacity = 0;
            ui.doc.style.opacity = 0;
            ui.status.style.opacity = 0;
            ui.bgm.root.pause();
            ui.brewFx.root.pause();
          }
        } else if (this.busy <= 0) {
          Scene.instance.gameOver = true;
          this.asleep = true;
          ui.finalWords.root.innerText = ui.doc.words + (ui.doc.words == 1 ? ' word' : ' words');
          ui.sleepTime.root.innerText = ui.doc.clock.innerText;
          ui.pauseButton.root.innerText = 'New Game';
          this.busy = 10000;
          this._updateState();
        }
      }
    }
    if (this.state === FILLING) {
      if (this.busy <= 0) {
        ui.brewMeter.value += ms * .0001;
        if (this.fill.hide) {
          this.fill.hide = false;
          this.fill.dirty = true;
        }
      }
    } else if (this.state === POURING) {
      ui.brewMeter.value -= ms * .0001;
      ui.coffeeMeter.value += ms * .0004;
      if (ui.coffeeMeter.value >= 1 || ui.brewMeter.value <= 0) {
        ui.coffeeMeter.value = 1;
        this.busy = 0;
        this._updateState(IDLE);
      }
    }
    if (this.drinking > 0) {
      this._updateMug();
      ui.coffeeMeter.value -= ms * .0003;
      if (this.drinking > 0.75) {
        this.caffeine += ms * .0003;
        if (this.caffeine < .05) {
          ui.energyMeter.value += ms * .0002;
        }
      }
      this.drinking -= ms * .00125;
      if (this.drinking < 0) {
        if (ui.coffeeMeter.value < 0.05) {
          ui.coffeeMeter.value = 0;
        }
        this.busy = 0;
        this._updateState();
      }
    }
    const brew = ui.brewMeter.value;
    const newImg = frames[(brew * 5)|0];
    if (newImg != this.pot.img) {
      this.pot.img = newImg;
      this.pot.dirty = true;
    }
    if (this.state === FILLING) {
      if (brew >= 1) {
        this.fill.hide = true;
        this.fill.dirty = true;
        this._updateState(IDLE);
      } else if (brew <= 0 && this.busy <= 0) {
        this._updateState(IDLE);
      }
    }
    if (this.busy > 0 && this.state !== POURING && this.drinking <= 0) {
      this.busy -= ms;
      if (this.busy <= 0) {
        this._updateState();
      }
    }
    this._updatePlayer(ms);
  }
};

const coffeepot = new CoffeePot();
export default coffeepot;
