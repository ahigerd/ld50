import './components.js';
import { loadDictionary } from './editor.js';
import coffeepot from './coffeepot.js';
import Scene from './scene.js';
import assets from './assets.js';
import ThinUI from './thinui.js';

function onResize() {
  if (!document.fullscreenElement) {
    Scene.ui.gameFS.style.zoom = 1;
  } else {
    const vZoom = screen.height / 466;
    const hZoom = screen.width / 768;
    if (vZoom > 1 && hZoom > 1) {
      Scene.ui.gameFS.style.zoom = 1;
    } else {
      Scene.ui.gameFS.style.zoom = vZoom > hZoom ? hZoom : vZoom;
    }
  }
}

window.addEventListener('resize', onResize);

document.addEventListener('fullscreenchange', () => {
  if (!Scene.instance.paused && !document.fullscreenElement) {
    Scene.instance.pause();
  }
  setTimeout(onResize, 0);
});

(async function init() {
  try {
    const loadPromise = new Promise(resolve => window.addEventListener('load', resolve));
    const bgmPromise = bgm.readyState >= 2 ? Promise.resolve() : new Promise(resolve => bgm.addEventListener('canplay', resolve));
    const promises = [assets, loadPromise, loadDictionary];
    for (const el in document.getElementsByTagName('AUDIO')) {
      if (el.readyState < 2) {
        promises.push(new Promise(resolve => el.addEventListener('canplay', resolve)));
      }
    }
    await Promise.all(promises);
    const ui = ThinUI.init(document.body);
    window.ui = ui;

    const settings = JSON.parse(localStorage.settings || '{}') || {};
    if (!('fullscreen' in settings)) {
      settings.fullscreen = screen.orientation.type.includes('portrait') !== (screen.orientation.angle % 180 === 90);
    }

    const settingsKeys = ['fullscreen', 'enableSfx', 'enableBgm', 'autoType'];
    function saveSettings() {
      setTimeout(() => {
        for (const key of settingsKeys) {
          settings[key] = ui[key].root.checked;
        }
        localStorage.settings = JSON.stringify(settings);
      }, 0);
    }
    for (const key of settingsKeys) {
      if (key in settings) {
        ui[key].root.checked = settings[key];
      }
      ui[key].root.addEventListener('input', saveSettings);
    }

    ui.typeButton.onClick = () => {
      ui.scene.resume();
      ui.doc.addWord();
    };
    ui.typeButton.onDblclick = event => event.preventDefault();
    ui.scene.registerTick(ui.doc);

    ui.pauseButton.root.onclick = () => {
      if (coffeepot.asleep) {
        ui.title.show();
        ui.startGame.focus();
      } else {
        ui.scene.pause(true);
      }
    };
    ui.startGame.root.onclick = () => {
      coffeepot.reset();
      ui.doc.reset();
      ui.scene.style.opacity = 1;
      ui.doc.style.opacity = 1;
      ui.status.style.opacity = 1;
      ui.title.hide();
      ui.scene.start();
    };
    window.addEventListener('keydown', event => event.keyCode == 27 && !coffeepot.asleep && ui.scene.pause(true));
    window.addEventListener('blur', () => ui.scene.pause());

    coffeepot.attach(ui);
    ui.scene.attach(ui);
    ui.loading.hide();
    ui.game.style.display = 'block';
    ui.startGame.focus();
  } catch (err) {
    const loading = document.getElementById('loading');
    let stackTrace = (err.stack || '').split('\n').slice(1).join('\n');
    if (stackTrace) {
      stackTrace = '<div style="font-size:12px;white-space:pre;text-align:left">' + stackTrace + '</div>';
    }
    loading.innerHTML = '<div>Loading Error! <div style="font-size:16px;text-align:center">' + (err.message || err) + '</div>' + stackTrace + '</div>';
    loading.style.display = '';
    document.getElementById('game').style.display = 'none';
  }
})();
