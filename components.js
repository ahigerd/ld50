import ThinUI, { ThinUIComponent } from './thinui.js';

export class IconButton extends ThinUIComponent {
  constructor(props, root) {
    super(props, root);
    this._disabled = false;
    this._onClick = this._onClick.bind(this);
    this._unpop = this._unpop.bind(this);
    this.root.addEventListener('click', this._onClick);
    if (props.domContent) {
      for (const node of props.domContent) {
        this.labelDiv.appendChild(node.cloneNode(true));
      }
    }
    this.accelerator = this.label[0].toLowerCase();
    window.addEventListener('keypress', this._onClick);
  }

  _unpop() {
    this.root.classList.remove('active');
  }

  _onClick(event) {
    if (this.syntheticOnly && !event.synthetic) {
      event.preventDefault && event.preventDefault();
      return;
    }
    if (event.type === 'keypress') {
      if (event.key.toLowerCase() !== this.accelerator || event.repeat) {
        return;
      }
      this.root.classList.add('active');
      setTimeout(this._unpop, 50);
    }
    event.preventDefault && event.preventDefault();
    if (!this._disabled && this.onClick) {
      this.onClick();
    }
  }

  click() {
    this._onClick({
      synthetic: true,
      type: 'keypress',
      key: this.accelerator,
      repeat: false,
    });
  }

  get icon() {
    return this._icon;
  }

  set icon(v) {
    this._icon = v;
    if (!this._disabled) {
      this.img.src = v;
    }
  }

  get iconDisabled() {
    return this._iconDisabled;
  }

  set iconDisabled(v) {
    this._iconDisabled = v;
    if (this._disabled) {
      this.img.src = v || this._icon;
    }
  }

  get disabled() {
    return !!this._disabled;
  }

  set disabled(v) {
    if (!!v === this._disabled) {
      return;
    }
    this._disabled = !!v;
    this.root.classList.toggle('disabled', this._disabled);
    if (this._disabled) {
      this.img.src = this._iconDisabled || this._icon;
    } else {
      this.img.src = this._icon;
    }
  }
}
window.IconButton = IconButton;
ThinUI.register(IconButton);

export class Meter extends ThinUIComponent {
  _value = null;

  constructor(props, root) {
    super(props, root);
    let v = props;
    if (typeof v === 'number' || (!!v && isFinite(v))) {
      this.value = Number(v);
    } else if (this.root.dataset.value) {
      this.value = this.root.dataset.value;
    }
    this.filling = false;
  }

  get value() {
    return this._value;
  }

  set value(v) {
    if (typeof v === 'string' && v.endsWith('%')) {
      this._value = Number(v.slice(0, -1)) * .01;
    } else {
      this._value = Number(v);
    }
    if (this._value < 0) {
      this._value = 0;
    } else if (this._value > 1) {
      this._value = 1;
    }
    const width = (this._value * 180) & ~0x3;
    this.meterBar.style.width = `${width}px`;
  }
}
ThinUI.register(Meter);

